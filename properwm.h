#ifndef PROPERWM_H
#define PROPERWM_H

#include <loft.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>

#ifdef XINERAMA
#include <X11/extensions/Xinerama.h>
#endif

#define BIT(N)        (1 << (N - 1))

#ifndef MAX
#define MAX(A,B)      (A > B ? A : B)
#endif

#define LENGTH(X)     (sizeof(X) / sizeof(X[0]))

#define TAG_MASK      ((1 << LENGTH(tags)) - 1)
#define BUTTON_MASK   (ButtonPressMask|ButtonReleaseMask)
#define MOUSE_MASK    (BUTTON_MASK|PointerMotionMask)
#define CLEAN_MASK(M) (M & ~(numlockmask|LockMask) & (ShiftMask|ControlMask|Mod1Mask|Mod2Mask|Mod3Mask|Mod4Mask|Mod5Mask))

#define SELTAGS(M)    (M)->seltags[(M)->si]
#define CURTAG(M)     (M)->curtags[(M)->si]
#define CURTAGW(M)    (M)->tags[CURTAG(M)]

#define ONSCREEN(C)   ((C)->tags & (C)->mon->seltags[(C)->mon->si])
#define WIDTH(C)      ((C)->w + (2 * (C)->bw))
#define HEIGHT(C)     ((C)->h + (2 * (C)->bw))
#define FL_WIDTH(C)   ((C)->fw + (2 * (C)->bw))
#define FL_HEIGHT(C)  ((C)->fh + (2 * (C)->bw))

typedef union Arg Arg;
typedef struct Button Button;
typedef struct Client Client;
typedef struct Key Key;
typedef struct Layout Layout;
typedef struct Monitor Monitor;
typedef struct Panel Panel;
typedef struct Rule Rule;
typedef struct Tag Tag;
typedef struct TagTemplate TagTemplate;
typedef struct TagLabel TagLabel;
typedef struct Monitor Monitor;

typedef void (LayoutFunction) (Monitor*);
typedef void (UserFunction) (const Arg*);

// Enums

enum {
  TOP,
  BOTTOM
};

enum {
  ATTACH_HEAD,
  ATTACH_BODY,
  ATTACH_TAIL,
  ATTACH_PREV,
  ATTACH_NEXT,
  N_ATTACH_POINTS
};

enum {
  VTILE,
  HTILE,
  VFOLD,
  HFOLD,
  MONOCLE,
  FLOATING,
  N_LAYOUTS
};

enum {
  STATE_USED =     1,
  STATE_CURRENT =  2,
  STATE_SELECTED = 4,
  STATE_URGENT =   8,
  STATE_INDICATE = 16,
};

enum {
  CLICK_TAG,
  CLICK_LAYOUT,
  CLICK_TITLE,
  CLICK_COUNTER,
  CLICK_STATUS,
  CLICK_ROOT,
  CLICK_CLIENT
};

enum {
  STRUT_TOP,
  STRUT_BOTTOM,
  STRUT_LEFT,
  STRUT_RIGHT
};

enum {
  FLOAT =    1,
  VIEW =     2,
  NO_FOCUS = 4,
  ADD_TAGS = 8
};

// Internal enums

enum {
  CursorNormal,
  CursorMove,
  CursorResize,
  CursorLast
};

enum {
  NetActiveWindow,
  NetClientList,
  NetSupported,
  NetWMName,
  NetWMState,
  NetWMFullscreen,
  NetWMWindowType,
  NetWMWindowTypeDialog,
  NetLast
};

enum {
  WMDelete,
  WMProtocols,
  WMState,
  WMTakeFocus,
  WMLast
};

// Types

union Arg {
  signed int i;
  unsigned int ui;
  float f;
  const void* v;
};

struct Button {
  int click;
  unsigned int mask;
  unsigned int button;
  UserFunction* func;
  const Arg arg;
};

struct Client {
  char name[512];
  unsigned int tags;

  bool fixed,
       floating,
       fullscreen,
       never_focus,
       urgent,
       was_floating;

  Monitor* mon;
  Window win;

  int bw,
      fx, fy,
      fw, fh,
      x, y,
      w, h,
      orig_bw,
      orig_x, orig_y,
      orig_w, orig_h;

  float min_a, max_a;
  int base_w, base_h,
      inc_w, inc_h,
      max_w, max_h,
      min_w, min_h;

  float cfactor;

  Client* prev;
  Client* next;

  Client* sprev;
  Client* snext;
};

struct Key {
  unsigned int mask;
  KeySym keysym;
  UserFunction* function;
  Arg arg;
};

struct Layout {
  char* str;
  LayoutFunction* func;
};

struct Monitor {
  int index,
      mx, my,
      mw, mh,
      wx, wy,
      ww, wh;

  Panel* panel;

  int attach_point,
      struts[4];

  Tag *tags;
  int n_tags;

  int padding;
  unsigned int si,
               curtags[2],
               seltags[2];

   Client *selected,
          *prev,
          *clients,
          *stack;

   Monitor* next;
};

struct Panel {
  TagLabel *tags;
  int position;

  LoftWindow window;
  LoftLayout lt_horiz,
             lt_tags;
  LoftLabel  layout,
             title,
             counter,
             status;
};

struct Rule {
  const char* class;
  const char* instance;
  const char* title;
  int monitor;
  int attach_point;
  unsigned int tags;
  unsigned int attrs;
};

struct Tag {
  int index;
  char *name;
  TagLabel *label;

  Client *selected;
  unsigned int state;

  Layout *layout;
  int nmaster;
  float mfactor;
};

struct TagTemplate {
  char *name;
  int layout,
      nmaster;
  float mfactor;
};

char* TAG_LABEL = "properwm-tag-label";

struct TagLabel {
  LoftWidget w;
  Tag* tag;

  struct {
    struct rgba_pair current,
                     selected,
                     urgent,
                     used,
                     unused;
  } colors;

  PangoLayout* _lt;
  int _ext[2];
};

// actions.c

void client_to_monitor (const Arg* arg);
void focus_client (const Arg* arg);
void focus_monitor (const Arg* arg);
void kill_client (const Arg* arg);
void move_mouse (const Arg* arg);
void push (const Arg* arg);
void quit (const Arg* arg);
void resize_mouse (const Arg* arg);
void restart (const Arg* arg);
void set_attach_point (const Arg* arg);
void set_cfactor (const Arg* arg);
void set_client_tags (const Arg* arg);
void set_layout (const Arg* arg);
void set_mfactor (const Arg* arg);
void set_nmaster (const Arg* arg);
void set_padding (const Arg* arg);
void set_tags (const Arg* arg);
void spawn (const Arg* arg);
void spawn_browser (const Arg* arg);
void toggle_client_tags (const Arg* arg);
void toggle_floating (const Arg* arg);
void toggle_focus (const Arg* arg);
void toggle_panel_position (const Arg* arg);
void toggle_panel_visible (const Arg* arg);
void toggle_tag_set (const Arg* arg);
void toggle_tags (const Arg* arg);
void zoom_swap (const Arg* arg);

// client.c

void grab_buttons (Client* c, bool focused);
void set_focus (Client* c);
void reset_focus (void);
void unfocus (Client *c);
void focus (Client* c);

void client_apply_rules (Client* c, Tag** tag, bool* view, int* point);
void client_apply_size_hints (Client* c, int* x, int* y, int* w, int* h);
void client_configure (Client* c);
Tag* client_first_tag (Client* c);
Atom client_get_atom_prop (Client* c, Atom prop);
void client_grab_buttons (Client* c, bool focused);
void client_move_to_monitor (Client* c, Monitor* m);
void client_resize (Client* c, int x, int y, int w, int h);
bool client_send_event (Client* c, Atom proto);
void client_set_border_width (Client* c, int border_width);
void client_set_floating (Client* c, bool floating);
void client_set_fullscreen (Client* c, bool fullscreen);
void client_set_state (Client* c, long state);
void client_set_tags (Client* c, unsigned int tm);
void client_set_urgent(Client* c, bool urgent);
void client_toggle_tags (Client* c, unsigned int tm);
void client_unmanage (Client* c, bool destroyed);
void client_update_name (Client* c);
void client_update_size_hints (Client* c);
void client_update_window_type (Client* c);
void client_update_wm_hints (Client* c);

void attach (Client* c, Tag* t, int point);
void attach_head (Client* c);
void attach_tail (Client* c);
void attach_body (Client* c, Tag* t);
void attach_prev (Client* c, Client* s);
void attach_next (Client* c, Client* s);

void detach (Client* c);
void attach_stack (Client* c);
void detach_stack (Client* c);

int clients_m_count (Client* c, unsigned int tm);
int clients_mt_count (Client* c, unsigned int tm);
int clients_mf_count (Client* c, unsigned int tm);

int clients_v_count (Client* c);
int clients_vt_count (Client* c);
int clients_vf_count (Client* c);

Client* last_visible (void);
Client* next_visible (Client* c);
Client* next_visible_stack (Client* c);

Client* nv_masked (Client* c, unsigned int tm);
Client* nvs_masked (Client* c, unsigned int tm);

Client* nv_floating (Client* c);
Client* nv_floating_masked (Client* c, unsigned int tm);
Client* nv_tiled (Client* c);
Client* nv_tiled_masked (Client* c, unsigned int tm);

Client* prev_visible (Client* c);
Client* pv_floating (Client* c);
Client* pv_tiled (Client* c);

Client* xwindow_to_client (Window w);

// layouts.c

void vtile (Monitor* m);
void htile (Monitor* m);
void vfold (Monitor* m);
void hfold (Monitor* m);
void monocle (Monitor* m);

// monitor.c

void arrange (Monitor* m);
void show_hide (Monitor* m);

void monitor_arrange (Monitor* m);
Monitor* monitor_create (int index);
void monitor_restack (Monitor* m);
void monitor_set_strut (Monitor* m, int strut, int px);
void monitor_set_tags (Monitor* m, unsigned int tm);
void monitor_toggle_tags (Monitor* m, unsigned int tm);
void monitor_update_geometry (Monitor* m);

Monitor* cycle_monitors (int d);

#ifdef XINERAMA
void init_monitors (XineramaScreenInfo* info, int n);
#endif
void setup_monitors (void);
bool update_monitors (void);

Monitor* xwindow_to_monitor (Window w);
Monitor* xy_to_monitor (int x, int y);

// panel.c

Panel* panel_create (Monitor* m);
void panel_destroy (Monitor* m);
void panel_refresh (Monitor* m);
void panel_sync_counter (Monitor* m);
void panel_sync_layout (Monitor* m);
void panel_sync_tags (Monitor* m);
void panel_sync_title (Monitor* m);
void refresh_status (void);
void show_hide_status (void);
void update_panels (void);

// xwindow.c

long xwindow_get_state (Window w);
bool xwindow_get_text_prop (Window w, Atom atom, char* text, int size);
void xwindow_manage (Window w, XWindowAttributes* wa);

// xevents.c

void on_button_press (XEvent* e);
void on_client_message (XEvent* e);
void on_configure_notify (XEvent* e);
void on_configure_request (XEvent* e);
void on_destroy_notify (XEvent* e);
void on_enter_notify (XEvent* e);
void on_expose (XEvent *e);
void on_focus_in (XEvent* e);
void on_key_press (XEvent *e);
void on_map_request (XEvent* e);
void on_mapping_notify (XEvent* e);
void on_motion_notify (XEvent* e);
void on_property_notify (XEvent* e);
void on_unmap_notify (XEvent *e);

// xmisc.c

long get_color (const char* colstr);
bool get_root_ptr (int* x, int* y);
void grab_keys (void);
void sig_child (int unused);
void update_client_list (void);
void update_numlock_mask (void);

// main.c

int xerror (Display* dpy, XErrorEvent* ee);
int xerror_dummy (Display* dpy, XErrorEvent* ee);

void properwm_init (void);
void scan_clients (void);
void process_events (void);
void cleanup (void);
void die (const char* errstr, ...);

#endif
