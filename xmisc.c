long get_color (const char *cstr) {
  XColor color;

  if(XAllocNamedColor(dpy, loft.colormap, cstr, &color, &color) == false)
    die("error allocating color: \"%s\"\n", cstr);

  return color.pixel;
}

bool get_root_ptr (int *x, int *y) {
  int di;
  unsigned int dui;
  Window dummy;

  return XQueryPointer(dpy, root, &dummy, &dummy, x, y, &di, &di, &dui);
}

void grab_keys (void) {
  update_numlock_mask();
  {
    int i,j;
    unsigned int modifiers[] = { 0, LockMask, numlockmask, numlockmask | LockMask };
    Key *key;
    KeyCode code;

    XUngrabKey(dpy, AnyKey, AnyModifier, root);

    for(i = 0; i < LENGTH(keys); ++i) {
      key = &keys[i];
      code = XKeysymToKeycode(dpy, key->keysym);

      if(code != 0) {
        for(j = 0; j < LENGTH(modifiers); j++)
          XGrabKey(dpy, code, key->mask | modifiers[j], root, true, GrabModeAsync, GrabModeAsync);
      }
    }
  }
}

void update_client_list (void) {
  XDeleteProperty(dpy, root, netatom[NetClientList]);

  Monitor *m; Client *c;
  for(m = mons; m != NULL; m = m->next) {
    for(c = m->clients; c != NULL; c = c->next) {
      XChangeProperty(dpy, root, netatom[NetClientList],
          XA_WINDOW, 32, PropModeAppend,
          (unsigned char *) &(c->win), 1);
    }
  }
}

void update_numlock_mask (void) {
  int i,j;
  XModifierKeymap *modmap;

  numlockmask = 0;
  modmap = XGetModifierMapping(dpy);

  for(i = 0; i < 8; ++i) {
    for(j = 0; j < modmap->max_keypermod; j++) {
      if(modmap->modifiermap[i *modmap->max_keypermod + j] == XKeysymToKeycode(dpy, XK_Num_Lock))
        numlockmask = (1 << i);
    }
  }

  XFreeModifiermap(modmap);
}
