void client_to_monitor (const Arg *arg) {
  if(selmon->selected == NULL ||
      selmon->selected->floating ||
      CURTAGW(selmon).layout->func == NULL ||
      mons->next == NULL)
  {
    return;
  }

  Monitor *m = cycle_monitors(arg->i);
  client_move_to_monitor(selmon->selected, m);
}

void focus_client (const Arg *arg) {
  if(selmon->selected == NULL || selmon->selected->fullscreen)
    return;

  Client *c;
  if(arg->i > 0) {
    c = next_visible(selmon->selected->next);
    if(c == NULL)
      c = next_visible(selmon->clients);
  }
  else {
    c = prev_visible(selmon->selected->prev);
    if(c == NULL)
      c = last_visible();
  }

  if(c != NULL && selmon->selected != c) {
    focus(c);
    panel_refresh(c->mon);
    monitor_restack(c->mon);
  }
}

void focus_monitor (const Arg *arg) {
  if(mons->next == NULL)
    return;

  Monitor *m = cycle_monitors(arg->i);
  if(m == selmon)
    return;

  if(selmon->selected != NULL)
    unfocus(selmon->selected);

  Monitor *oldmon = selmon;
  selmon = m;

  focus(CURTAGW(selmon).selected);
  panel_refresh(oldmon);
  panel_refresh(selmon);
  show_hide_status();
}

void kill_client (const Arg *arg) {
  if(selmon->selected == NULL)
    return;

  Client *c = selmon->selected;

  if(client_send_event(c, wmatom[WMDelete]) == false) {
    XGrabServer(dpy);
    XSetErrorHandler(xerror_dummy);
    XSetCloseDownMode(dpy, DestroyAll);
    XKillClient(dpy, c->win);
    XSync(dpy, false);
    XSetErrorHandler(xerror);
    XUngrabServer(dpy);
  }
}

void move_mouse (const Arg *arg) {
  Client *c = selmon->selected;
  if(c == NULL || c->fullscreen)
    return;

  Tag *ct = &CURTAGW(selmon);

  int ix = c->fx;
  int iy = c->fy;

  monitor_restack(selmon);
  XWarpPointer(dpy, None, root, 0, 0, 0, 0, c->fx + (FL_WIDTH(c) / 2), c->fy + (FL_HEIGHT(c) / 2));

  if(XGrabPointer(dpy, root, false, MOUSE_MASK, GrabModeAsync, GrabModeAsync, None, cursor[CursorMove], CurrentTime) != GrabSuccess)
    return;

  int px;
  int py;

  if(get_root_ptr(&px, &py) == false)
    return;

  XEvent ev;

  int nx;
  int ny;

  while(ev.type != ButtonRelease) {
    XMaskEvent(dpy, ExposureMask|PropertyChangeMask|StructureNotifyMask|SubstructureRedirectMask|MOUSE_MASK, &ev);

    switch (ev.type) {
      case MotionNotify:
        nx = ix + (ev.xmotion.x - px);
        ny = iy + (ev.xmotion.y - py);

        if(c->floating || ct->layout->func == NULL) {
          if(nx > selmon->wx && nx < selmon->wx + snap_threshold)
            nx = selmon->wx;
          else if(nx + FL_WIDTH(c) < selmon->wx + selmon->ww && nx + FL_WIDTH(c) > selmon->wx + selmon->ww - snap_threshold)
            nx = selmon->wx + selmon->ww - FL_WIDTH(c);

          if(ny > selmon->wy && ny < selmon->wy + snap_threshold)
            ny = selmon->wy;
          else if(ny + FL_HEIGHT(c) < selmon->wy + selmon->wh && ny + FL_HEIGHT(c) > selmon->wy + selmon->wh - snap_threshold)
            ny = selmon->wy + selmon->wh - FL_HEIGHT(c);

          client_resize(c, nx, ny, c->fw, c->fh);
          c->fx = c->x;
          c->fy = c->y;
        }
        else if(abs(ev.xmotion.x - px) > snap_threshold || abs(ev.xmotion.y - py) > snap_threshold) {
          toggle_floating(NULL);
        }

        break;

      case Expose:
      case ConfigureNotify:
      case MapNotify:
      case UnmapNotify:

        loft_process(&ev);
        break;

      case ConfigureRequest:
      case MapRequest:
      case PropertyNotify:

        handlers[ev.type](&ev);
        break;
    }
  }

  XUngrabPointer(dpy, CurrentTime);
  XSync(dpy, false);

  Monitor *m = xy_to_monitor(c->x + (WIDTH(c) / 2), c->y + (HEIGHT(c) / 2));
  if(m != NULL && m != c->mon) {
    client_move_to_monitor(c, m);
    selmon = c->mon;
  }
}

void push (const Arg *arg) {
  Client *c = selmon->selected;
  if(c == NULL)
    return;

  Client *s;

  detach(c);
  if(arg->i > 0) {
    s = next_visible(c->next);
    if(s != NULL)
      attach_next(c,s);
    else
      attach_head(c);
  }
  else {
    s = prev_visible(c->prev);
    if(s != NULL)
      attach_prev(c,s);
    else
      attach_tail(c);
  }

  arrange(selmon);
  panel_refresh(selmon);
}

void quit (const Arg *arg) {
  ret = 0;
  loft_break();
}

void resize_mouse (const Arg *arg) {
  Client *c = selmon->selected;
  if(c == NULL || c->fullscreen)
    return;

  int ix = c->fx;
  int iy = c->fy;

  monitor_restack(selmon);
  XWarpPointer(dpy, None, root, 0, 0, 0, 0, c->fx + c->bw + c->fw, c->fy + c->bw + c->fh);

  if(XGrabPointer(dpy, root, false, MOUSE_MASK, GrabModeAsync, GrabModeAsync, None, cursor[CursorResize], CurrentTime) != GrabSuccess)
    return;

  XEvent ev;
  int nw, nh;

  while(ev.type != ButtonRelease) {
    XMaskEvent(dpy, ExposureMask|PropertyChangeMask|StructureNotifyMask|SubstructureRedirectMask|MOUSE_MASK, &ev);

    switch (ev.type) {
      case MotionNotify:
        nw = ev.xmotion.x - ix;
        nh = ev.xmotion.y - iy;

        if(c->floating || CURTAGW(c->mon).layout->func == NULL) {
          client_resize(c, c->fx, c->fy, nw, nh);
          c->fw = c->w;
          c->fh = c->h;
        }
        else if(abs(nw - c->fw) > snap_threshold || abs(nh - c->fh) > snap_threshold) {
          toggle_floating(NULL);
        }

        break;

      case Expose:
      case ConfigureNotify:
      case MapNotify:
      case UnmapNotify:
        loft_process(&ev);
        break;

      case ConfigureRequest:
      case MapRequest:
      case PropertyNotify:
        handlers[ev.type](&ev);
        break;
    }
  }

  while(XCheckMaskEvent(dpy, EnterWindowMask, &ev));

  XUngrabPointer(dpy, CurrentTime);
  XSync(dpy, false);

  Monitor *m = xy_to_monitor(c->x + (WIDTH(c) / 2), c->y + (HEIGHT(c) / 2));
  if(m != NULL && m != selmon) {
    client_move_to_monitor(c, m);
    selmon = c->mon;
  }
}

void restart (const Arg *arg) {
  extern bool _restart;
  _restart = true;
  loft_break();
}

void set_attach_point (const Arg *arg) {
  if(arg->i == 0) {
    selmon->attach_point = init_attach_point;
    panel_refresh(selmon);
    return;
  }

  int ap = (selmon->attach_point + arg->i) % N_ATTACH_POINTS;
  if(ap < 0)
    ap += N_ATTACH_POINTS;

  selmon->attach_point = ap;
  panel_refresh(selmon);
}

void set_cfactor (const Arg *arg) {
  Client *c = selmon->selected;
  if(c == NULL || c->floating || CURTAGW(selmon).layout->func == NULL)
    return;

  if(arg->f == 0.0) {
    c->cfactor = 1.0;
    arrange(selmon);
    return;
  }

  float new = c->cfactor + arg->f;
  if(new < 0.249 || new > 4.0)
    return;

  c->cfactor = new;
  arrange(selmon);
}

void set_client_tags (const Arg *arg) {
  if(selmon->selected == NULL)
    return;

  client_set_tags(selmon->selected, arg->ui);
  panel_refresh(selmon);
}

void set_layout (const Arg *arg) {
  Tag *ct = &CURTAGW(selmon);
  if(arg->v == ct->layout)
    return;

  ct->layout = (Layout*) arg->v;
  arrange(selmon);
  panel_refresh(selmon);
}

void set_mfactor (const Arg *arg) {
  Tag *ct = &CURTAGW(selmon);
  if(arg == NULL || ct->layout->func == NULL || nv_tiled(selmon->clients) == NULL)
    return;

  if(arg->f == 0.0) {
    ct->mfactor = tags[CURTAG(selmon)].mfactor;
    arrange(selmon);
    return;
  }

  float new = arg->f + ct->mfactor;
  if(new < 0.1 || new > 0.9)
    return;

  ct->mfactor = new;
  arrange(selmon);
}

void set_nmaster (const Arg *arg) {
  Tag *ct = &CURTAGW(selmon);
  if(ct->layout->func == NULL)
    return;

  if(arg->i == 0) {
    ct->nmaster = tags[CURTAG(selmon)].nmaster;
    arrange(selmon);
    return;
  }

  int new = ct->nmaster + arg->i;
  if(new < -1)
    return;

  ct->nmaster = new;
  arrange(selmon);
}

void set_padding (const Arg *arg) {
  if(arg->i == 0) {
    selmon->padding = padding;
    return;
  }

  int new = selmon->padding + arg->i;
  if(new < 0)
    return;

  selmon->padding = new;
  arrange(selmon);
}

void set_tags (const Arg *arg) {
  monitor_set_tags(selmon, arg->ui);
  monitor_restack(selmon);
  panel_refresh(selmon);
}

void spawn (const Arg *arg) {
  if(fork() == 0) {
    char **cmd = (char**) arg->v;

    if(dpy)
      close(ConnectionNumber(dpy));

    setsid();
    if(execvp(cmd[0], cmd) == -1) {
      fprintf(stderr, "properwm: execvp %s", cmd[0]);
      perror(" failed"); // only prints if error
      exit(0);
    }
  }
}

void spawn_browser (const Arg *arg) {
  char *cmd[2];
  const Arg a = { .v = cmd };

  cmd[0] = getenv("BROWSER");
  cmd[1] = NULL;

  spawn(&a);
}

void toggle_client_tags (const Arg *arg) {
  if(selmon->selected == NULL)
    return;

  client_toggle_tags(selmon->selected, arg->ui);
  panel_refresh(selmon);
}

void toggle_floating (const Arg *arg) {
  Client *c = selmon->selected;
  if(c == NULL || c->fullscreen || c->fixed || CURTAGW(selmon).layout->func == NULL)
    return;

  c->floating ^= true;
  arrange(selmon);
}

void toggle_focus (const Arg *arg) {
  if(selmon->selected == NULL)
    return;

  focus(next_visible_stack(selmon->selected->snext));
  arrange(selmon);
  panel_refresh(selmon);
}

void toggle_panel_position (const Arg *arg) {
  Panel *p = selmon->panel;
  if(p->window.w.visible == false)
    return;

  int ny;
  if(p->position == TOP) {
    ny = selmon->my + selmon->mh - p->window.w.height;
    selmon->struts[STRUT_TOP] -= p->window.w.height;
    selmon->struts[STRUT_BOTTOM] += p->window.w.height;
    p->position = BOTTOM;
  }
  else {
    ny = selmon->my;
    selmon->struts[STRUT_TOP] += p->window.w.height;
    selmon->struts[STRUT_BOTTOM] -= p->window.w.height;
    p->position = TOP;
  }

  LoftWidget *w;
  for(w = &selmon->panel->tags->w; w != NULL; w = w->next)
    w->dirty = true;

  loft_window_move(&p->window, selmon->mx, ny);

  monitor_update_geometry(selmon);
  arrange(selmon);
  panel_refresh(selmon);
}

void toggle_panel_visible (const Arg *arg) {
  Panel *p = selmon->panel;
  if(p->window.w.visible == false)
    loft_window_show(&p->window);
  else
    loft_window_hide(&p->window);
}

void toggle_tag_set (const Arg *arg) {
  selmon->si ^= 1;

  focus(CURTAGW(selmon).selected);
  arrange(selmon);
  panel_refresh(selmon);
}

void toggle_tags (const Arg *arg) {
  monitor_toggle_tags(selmon, arg->ui);
  panel_refresh(selmon);
}

void zoom_swap (const Arg *arg) {
  Tag *ct = &CURTAGW(selmon);

  if(ct->layout->func == NULL)
    return;

  Client *f = ct->selected;
  if(f == NULL || f->floating)
    return;

  Client *pf = next_visible_stack(f->snext);
  if(pf == NULL || pf->floating)
    return;

  Client *head = next_visible(selmon->clients);

  if(f == head) {
    detach(f);
    attach_next(f, pf->prev);

    detach(pf);
    attach_head(pf);
  }
  else if(pf == head) {
    detach(pf);
    attach_next(pf, f->prev);

    detach(f);
    attach_head(f);
  }
  else {
    detach(f);
    attach_head(f);
  }

  arrange(selmon);
  panel_refresh(selmon);
}
