BIN = properwm
VERSION = $(shell printf 2)

CFLAGS = -g -O0 -Wall -Wno-unused-label -fPIC --std=c11 `pkg-config --cflags loft`
CPPFLAGS = -D_DEFAULT_SOURCE -DVERSION=$(VERSION)
LDLIBS = -lm `pkg-config --libs loft`

DESTDIR =
PREFIX = $(DESTDIR)/usr/local
BINDIR = $(PREFIX)/bin

all: $(BIN)

config:
	if [ ! -e config.h ]; then cp config.def.h config.h; fi

$(BIN): config
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDLIBS) $(BIN).c -o $(BIN)

install: $(BIN)
	cp -f $(BIN) $(BINDIR)/

uninstall:
	rm -f $(BINDIR)/$(BIN)

clean:
	rm -f $(BIN)
	rm -f config.h

.PHONY: all install uninstall clean
